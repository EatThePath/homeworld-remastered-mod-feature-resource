version = 4

priority = 99
envelope = {
  {
    distance = 0.000000,
    volume = 1,
    reverb = 0.000000,
    duration = 0,
    equalizer = {
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
    }
  },
  {
    distance = 866.666687,
    volume = 1,
    reverb = 0.000000,
    duration = 0,
    equalizer = {
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
    }
  },
  {
    distance = 2216.666748,
    volume = 1,
    reverb = 0.000000,
    duration = 0,
    equalizer = {
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
    }
  },
  {
    distance = 5916.666504,
    volume = 1,
    reverb = 0.000000,
    duration = 0,
    equalizer = {
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
    }
  },
  {
    distance = 7550.000000,
    volume = 1.000000,
    reverb = 0.000000,
    duration = 0,
    equalizer = {
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
              1.000000,
    }
  },
}
container = 0
playlist = 0
randContainer = 0
loopingPlaylist = 0
silenceBreak = 0.000000
silenceRandom = 0.000000
randSampContainer = 0