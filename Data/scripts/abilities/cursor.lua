-----------------------------------------------------------------------------------
--Functions for addAbility CustomCommands
-----------------------------------------------------------------------------------

--Run when ability is activated
function BeginAB_Cursor(CustomGroup, playerIndex, shipID)
    SobGroup_MakeSelectable(CustomGroup,0)
    SobGroup_AbilityActivate(CustomGroup, AB_Move, 0)
    SobGroup_Stop(playerIndex, CustomGroup)
    SobGroup_DeSelectAll()
end

--Run periodically durring ability
function DuringAB_Cursor(CustomGroup, playerIndex, shipID)
    if(SobGroup_IsSelectable(CustomGroup)==1) then
        SobGroup_MakeSelectable(CustomGroup,0)
    end
end

--Run at the end of the ability
function EndAB_Cursor(CustomGroup, playerIndex, shipID)
end

-----------------------------------------------------------------------------------
-- Functions for addCustomCode code
-----------------------------------------------------------------------------------

--Run at ship creation
function OnCreate_Cursor(CustomGroup, playerIndex, shipID)

end

--Run periodicly while the ship is alive
function OnUpdate_Cursor(CustomGroup, playerIndex, shipID)

end

--Run when the ship dies
function OnDestroy_Cursor(CustomGroup, playerIndex, shipID)
end
