-----------------------------------------------------------------------------------
--Ship state
--ShipID keyed tables are good in my experince -EatThePath
-----------------------------------------------------------------------------------
ABT_Siege = {}
-----------------------------------------------------------------------------------
--Functions for addAbility CustomCommands
-----------------------------------------------------------------------------------
AB_SIEGE_STATE_INACTIVE = 0
AB_SIEGE_STATE_WAITING = 1
AB_SIEGE_STATE_FIRING = 2
AB_SIEGE_STATE_COOLOFF = 3
--Run when ability is activated
function BeginAB_SiegeCannon(CustomGroup, playerIndex, shipID)
    SobGroup_Stop(playerIndex,CustomGroup)
    --spawn cursor and put it in the storage group
    ABT_Siege[shipID].cursorSob = SobGroup_CreateShip(CustomGroup, "_cursor")
    --flag state as active
    ABT_Siege[shipID].state = AB_SIEGE_STATE_WAITING
    ABT_Siege[shipID].fireTicks = 0

    ABT_Siege[shipID].selected = 0

    --make ship unselectable
    SobGroup_DeSelectAll()
    SobGroup_MakeSelectable(CustomGroup,0)
    --Disable the special ability, because we're going to do everything in the custom code functions.
    SobGroup_AbilityActivate(CustomGroup,AB_Custom,0)
end

--Run periodically durring ability
function DuringAB_SiegeCannon(CustomGroup, playerIndex, shipID)

end

--Run at the end of the ability
function EndAB_SiegeCannon(CustomGroup, playerIndex, shipID)
end

-----------------------------------------------------------------------------------
-- Functions for addCustomCode code
-----------------------------------------------------------------------------------

--Runs at load time
function OnLoad_SiegeCannon(playerIndex)
    --At first it seems like a good idea to reinitalize the global state table here, because it is known to
    --sometimes persist between games. However, all my attempts to do this have ended up infuriatingly making
    --the state table somehow a local variable of this function and then hiding the global version of it from
    --all the other functions in the script. This naturally breaks everything.
    --Consequently, just be tidy about how the table is handled in OnCreate, and don't do either of the
    --commented out things below.

    --Attempt one
    --ABT_Siege = {} --This is what you would think the right thing to do would be
    --attempt two
    --if(ABT_Siege ~= nil) then
        --for key,value in ABT_Siege do
            --ABT_Siege[key] = nil
        --end
    --end
end

--Run at ship creation
function OnCreate_SiegeCannon(CustomGroup, playerIndex, shipID)
    --create this ship's indivudal siege state subtable.
    ABT_Siege[shipID] = {}
    ABT_Siege[shipID].state = AB_SIEGE_STATE_INACTIVE
end

--Run periodicly while the ship is alive
function OnUpdate_SiegeCannon(CustomGroup, playerIndex, shipID)
    --The body of the siege ability handling is done here, because making cooldown and cancelation work doesn't
    --work with putting it in the ability update function.
    
    if(ABT_Siege[shipID].state == AB_SIEGE_STATE_INACTIVE)then
        --Disables the siege weapon while the ship is in the inactive state
        --reasonably speaking this should be done in OnCreate and at state transitions, however
        --for some reason doing it onCreate doesn't work, so we do it here.
        SobGroup_ChangePower(CustomGroup,"Weapon_Gun0",0)
    end

    --keep track of how many simticks have passed since the fire command has been given
    if(ABT_Siege[shipID].state == AB_SIEGE_STATE_FIRING or ABT_Siege[shipID].state == AB_SIEGE_STATE_COOLOFF ) then
        ABT_Siege[shipID].fireTicks = ABT_Siege[shipID].fireTicks + 1
    end
    --select cursor as soon as possible
    if(ABT_Siege[shipID].selected == 0 and SobGroup_IsSelectable(ABT_Siege[shipID].cursorSob)== 1 ) then
        SobGroup_SelectSobGroup(ABT_Siege[shipID].cursorSob)
        ABT_Siege[shipID].selected =1
    end

    --IDEA: Fire on the cursor with a long range invisibeam to make the firing ship always be aiming at it?
    --Needs a different hod than we are currently using

    --The waiting phase behavior
    if(ABT_Siege[shipID].state == AB_SIEGE_STATE_WAITING) then
        --if cursor dies, return ship to normal
        if(SobGroup_Count(ABT_Siege[shipID].cursorSob) <= 0) then
            AB_SiegeCannon_Cleanup(CustomGroup, playerIndex, shipID)
            ABT_Siege[shipID].state = AB_SIEGE_STATE_INACTIVE
            SobGroup_AbilityActivate(CustomGroup,AB_Custom,1)
            return
            --otherwise check to see if it is time to start the firing sequence
        elseif(SobGroup_CanDoAbility(ABT_Siege[shipID].cursorSob, AB_Move)==0) then
            ABT_Siege[shipID].state = AB_SIEGE_STATE_FIRING
            SobGroup_Attack(playerIndex,CustomGroup,ABT_Siege[shipID].cursorSob)
            --enable the seige cannon
            SobGroup_ChangePower(CustomGroup,"Weapon_Gun0",1)
        end
    end
    --The firing state covers what happens after the player triggers the fire ability on the cursor
    --The ship is given time to slew to target and fire, then is made reselectable
    --The ship stays in the firing state until the maximum flight time of the projectile plus the slew time is exceeded
    --At that point the cursor is cleaned up if it still exists
    if(ABT_Siege[shipID].state == AB_SIEGE_STATE_FIRING) then
        --So, after enough time has passed that the shot should have landed or not, kill the cursor ship if it still exists
        if( ABT_Siege[shipID].fireTicks >= 70) then
            if(SobGroup_Count(ABT_Siege[shipID].cursorSob) > 0 ) then
                SobGroup_SetHealth(ABT_Siege[shipID].cursorSob,0)
                AB_SiegeCannon_Cleanup(CustomGroup, playerIndex, shipID)
                ABT_Siege[shipID].fireTicks = 0
                ABT_Siege[shipID].state = AB_SIEGE_STATE_COOLOFF
            else
                ABT_Siege[shipID].state = AB_SIEGE_STATE_COOLOFF
            end
            --Failing that, after enough time the shot should absolutely have been fired, so then
            --disable the cannon and return to normal usability
        elseif( ABT_Siege[shipID].fireTicks >= 12) then
            if(SobGroup_IsSelectable(CustomGroup)==0) then
                SobGroup_MakeSelectable(CustomGroup,1)
                SobGroup_Stop(playerIndex,CustomGroup)
            end
            SobGroup_ChangePower(CustomGroup,"Weapon_Gun0",0)
        end
    end
    --keep track of the cooldown 
    if(ABT_Siege[shipID].state == AB_SIEGE_STATE_COOLOFF) then
        if(ABT_Siege[shipID].fireTicks >= 240) then
            SobGroup_AbilityActivate(CustomGroup,AB_Custom,1)
            AB_SiegeCannon_Cleanup(CustomGroup, playerIndex, shipID)
            ABT_Siege[shipID].fireTicks = 0
        end
    end
end

function AB_SiegeCannon_Cleanup(CustomGroup, playerIndex, shipID)
    SobGroup_MakeSelectable(CustomGroup,1)
    print("EndABT_SiegeCannon making "..shipID.." selectable")
    ABT_Siege[shipID].selected = false
end

--Run when the ship dies
function OnDestroy_SiegeCannon(CustomGroup, playerIndex, shipID)
    --Kill any cursor that exists
    if(ABT_Siege[shipID].cursorSob~= nil) then
        if(SobGroup_Count(ABT_Siege[shipID].cursorSob) > 0 ) then
            SobGroup_SetHealth(ABT_Siege[shipID].cursorSob,0)
        end
    end
    --clean up our entry in the state table.
    ABT_Siege[shipID] = nil
end
