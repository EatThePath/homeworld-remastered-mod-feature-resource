-----------------------------------------------------------------------------------
--Ship state
--ShipID keyed tables are good in my experince -EatThePath
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
--Functions for addAbility CustomCommands
-- .ship line
-- addAbility(NewShipType,"CustomCommand", --Standard ability stuff
--      1,"",   --initial ability availablilty(1=enabled, 0=disabled), ?string name of ability?
--      1,0, --??,?Values other than zero have only generated access violations in tests?
--      2400,2400, -- Maximum energy amount, amount of energy that must be spent before ability can be turned off
--      120,120,2400, -- activated energy burn rate,deactivated energy regen rate,?min activation energy? (rates are 1/10th of second)
--      "data:scripts/abilities/template.lua", -- path to ability code file. ensure has the same case as any other calls to this file, or it will be read more than once
--      "BeginAB_","DuringAB_","EndAB_", -- function names
--      "CustomCommand_Sob_",       --A name for the sobgroup passed to the functions. Seems wise to make sure it doesn't share a name with any sob names used in other scripts
--      -Otherwise the name seems unimportant but DO NOT LEAVE BANK, or the function apparently gets passed a Player_ShipsN group--Contains the ship name in most stock cases but seems unused
--      1.15,   -- (uncertain) how often the during function is executed
--      0,      -- index for UI and hotkey
--      1,      -- 'latent' flag, if true(1) this custom command can run while another command is active (used to allow commands to keep running while you move ships, attack, etc). (per Liuruoyang1995/Lone Wolf Akela)
--      0)      -- bool, if 1 ship dies when ability is depleted
-----------------------------------------------------------------------------------

--Run when ability is activated
function BeginAB_(CustomGroup, playerIndex, shipID)
end

--Run periodically durring ability
function DuringAB_(CustomGroup, playerIndex, shipID)
end

--Run at the end of the ability
function EndAB_(CustomGroup, playerIndex, shipID)
end

-----------------------------------------------------------------------------------
-- Functions for addCustomCode code
-- .ship line
-- addCustomCode(NewShipType,"data:scripts/abilities/template.lua", -- path to ability code file. ensure has the same case as any other calls to this file, or it will be read more than once
--     "OnLoad_","OnCreate_","OnUpdate_","OnDestroy_",
--     "thing", --A name for the sobgroup passed to the functions. Seems wise to make sure it doesn't share a name with any sob names used in other scripts
--              --Otherwise the name seems unimportant but DO NOT LEAVE BANK, or the function apparently gets passed a Player_ShipsN group
--     1.15) -- time between update executions, set below 0.05 to run every simframe
-----------------------------------------------------------------------------------

--Runs at load time, typically during the loading screen.
--Be aware this can run multiple times, likely once per player
--Also be aware that if the player starts with a ship you get a sequence like
--OnLoad_(0), OnCreate_(group,0,0), OnLoad_(1)
--As a consequence doing any intializing in the onload is dangerous and must be taken done with care.
--Just handle your state table with care in OnCreate_ and leave OnLoad alone, I think. -EatThePath
function OnLoad_(playerIndex)
end


--Run at ship creation
function OnCreate_(CustomGroup, playerIndex, shipID)

end

--Run periodicly while the ship is alive
function OnUpdate_(CustomGroup, playerIndex, shipID)

end

--Run when the ship dies
function OnDestroy_(CustomGroup, playerIndex, shipID)

end
