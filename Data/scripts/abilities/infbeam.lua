dofilepath("data:library/MFR.lua")

--Volume_AddSphere is available in this scope, needed for beam drawing
--check if that actually works

--FX_PlayEffectBetweenPoints is why we need it

--SobGroup_GetPosition hopefully can work with that.

--SobGroup_GetShipType is a key part

-----------------------------------------------------------------------------------
--Ship state
--ShipID keyed tables are good in my experince -EatThePath
-----------------------------------------------------------------------------------
if ABT_InfBeam == nil then
    ABT_InfBeam = {}
end
-----------------------------------------------------------------------------------
--Configuration constants
-----------------------------------------------------------------------------------
AB_InfBeam_Juice = 10
AB_InfBeam_FirstJumpRange = 3048 --Cata units treated like feat while hwr units are meters
AB_InfBeam_ChainJumpRange = 609.6
AB_InfBeam_PreJumpDelay = 0.55
AB_InfBeam_PostJumpDelay = 10.55
AB_InfBeam_BeamLifetime = 1
AB_InfBeam_SplitMethod = "DockFamily"
AB_InfBeam_CostTable = {}
AB_InfBeam_CostTable.Fighter = 1
AB_InfBeam_CostTable.Corvette = 2
AB_InfBeam_CostTable.Utility = 2
AB_InfBeam_CostTable.Frigate = 5
-----------------------------------------------------------------------------------
--Enumeration state symbols
-----------------------------------------------------------------------------------
IBEAM_VICTIMSTAGE_WAITINGBEAM = 0
IBEAM_VICTIMSTAGE_PREJUMP = 1
IBEAM_VICTIMSTAGE_POSTJUMP = 2
IBEAM_VICTIMSTAGE_DONE = 3
-----------------------------------------------------------------------------------
--Functions for addAbility CustomCommands
-- .ship line
-- addAbility(NewShipType,"CustomCommand", --Standard ability stuff
--      1,"",   --initial ability availablilty(1=enabled, 0=disabled), ?string name of ability?
--      1,0, --??,?Values other than zero have only generated access violations in tests?
--      2400,2400, -- Maximum energy amount, amount of energy that must be spent before ability can be turned off
--      120,120,2400, -- activated energy burn rate,deactivated energy regen rate,?min activation energy? (rates are 1/10th of second)
--      "data:scripts/abilities/template.lua", -- path to ability code file.
--      "BeginAB_","DuringAB_","EndAB_", -- function names
--      "CustomCommand_Sob_",       --A name for the sobgroup passed to the functions. Seems wise to make sure it doesn't share a name with any sob names used in other scripts
--      -Otherwise the name seems unimportant but DO NOT LEAVE BANK, or the function apparently gets passed a Player_ShipsN group--Contains the ship name in most stock cases but seems unused
--      1.15,   -- (uncertain) how often the during function is executed
--      0,      -- index for UI and hotkey
--      1,      -- 'latent' flag, if true(1) this custom command can run while another command is active (used to allow commands to keep running while you move ships, attack, etc). (per Liuruoyang1995/Lone Wolf Akela)
--      0)      -- bool, if 1 ship dies when ability is depleted
-----------------------------------------------------------------------------------


--Run when ability is activated
function BeginAB_InfBeam(CustomGroup, playerIndex, shipID)
    ABT_InfBeam[shipID].victims = {}
    ABT_InfBeam[shipID].juiceLeft = AB_InfBeam_Juice
    
    local targets = MFR_CreateSafeSob()
    SobGroup_GetCommandTargets(targets,CustomGroup,COMMAND_Attack)
    SobGroup_FillProximitySobGroup(targets,targets,CustomGroup,AB_InfBeam_FirstJumpRange)

    --Disable the special ability, because we're going to do everything in the custom code functions.
    SobGroup_AbilityActivate(CustomGroup,AB_Custom,0)
    if(SobGroup_Count(targets)>0) then 
        if(InfBeam_Jump(CustomGroup, playerIndex, shipID,AB_InfBeam_FirstJumpRange,targets) == 1) then
            FX_StartEvent(CustomGroup,"Infection")
            ABT_InfBeam[shipID].lastFireTime = Universe_GameTime()
            SobGroup_AbilityActivate(CustomGroup,AB_Custom,0) --This appears to instantly end execution of this function when called, fyi
        else
            ABT_InfBeam[shipID].juiceLeft = 0
        end
    else
        if(InfBeam_Jump(CustomGroup, playerIndex, shipID,AB_InfBeam_FirstJumpRange) == 1) then
            FX_StartEvent(CustomGroup,"Infection")
            ABT_InfBeam[shipID].lastFireTime = Universe_GameTime()
            SobGroup_AbilityActivate(CustomGroup,AB_Custom,0) --This appears to instantly end execution of this function when called, fyi
        else
            ABT_InfBeam[shipID].juiceLeft = 0
        end
    end
end

--Run periodically durring ability
function DuringAB_InfBeam(CustomGroup, playerIndex, shipID)
end

--Run at the end of the ability
function EndAB_InfBeam(CustomGroup, playerIndex, shipID)
end

-----------------------------------------------------------------------------------
-- Functions for addCustomCode code
-- .ship line
-- addCustomCode(NewShipType,"data:scripts/abilities/template.lua",
--     "OnLoad_","OnCreate_","OnUpdate_","OnDestroy_",
--     "thing", --A name for the sobgroup passed to the functions. Seems wise to make sure it doesn't share a name with any sob names used in other scripts
--              --Otherwise the name seems unimportant but DO NOT LEAVE BANK, or the function apparently gets passed a Player_ShipsN group
--     1.15) -- time between update executions probably
-----------------------------------------------------------------------------------

--Runs at load time, typically during the loading screen.
--Be aware this can run multiple times, likely once per player
--Also be aware that if the player starts with a ship you get a sequence like
--OnLoad_(0), OnCreate_(group,0,0), OnLoad_(1)
--As a consequence doing any intializing in the onload is dangerous and must be taken done with care.
--Just handle your state table with care in OnCreate_ and leave OnLoad alone, I think. -EatThePath
function OnLoad_InfBeam(playerIndex)
end

--Run at ship creation for ships with a 2 minute cooldown
function OnCreate_InfBeam(CustomGroup, playerIndex, shipID)
    ABT_InfBeam[shipID] = {}
    ABT_InfBeam[shipID].victims = {}
    ABT_InfBeam[shipID].victimIndex = -1
    ABT_InfBeam[shipID].beams = {}
    ABT_InfBeam[shipID].juiceLeft = 0
    ABT_InfBeam[shipID].rechargeTime = 120
    ABT_InfBeam[shipID].lastFireTime = 0
end
--Run at ship creation for ships with a 1.5 minute cooldown
function OnCreate_InfBeam90(CustomGroup, playerIndex, shipID)
    ABT_InfBeam[shipID] = {}
    ABT_InfBeam[shipID].victims = {}
    ABT_InfBeam[shipID].victimIndex = -1
    ABT_InfBeam[shipID].beams = {}
    ABT_InfBeam[shipID].juiceLeft = 0
    ABT_InfBeam[shipID].rechargeTime = 90
    ABT_InfBeam[shipID].lastFireTime = 0
end

function InfBeam_Jump(CustomGroup, playerIndex, shipID,fRange,sTargetGroup)
    --Check to see if we have enough range to make another jump
    --Get the 1 nearest infectable ship
    local newVictim = InfBeam_FindJumpTarget(CustomGroup, playerIndex, shipID,fRange,sTargetGroup)
    if newVictim == nil then
        --do the stuff to make us stop jumping
        ABT_InfBeam[shipID].juiceLeft = 0
        return 0
    else
        --jump to a new thing
        ABT_InfBeam[shipID].victimIndex = ABT_InfBeam[shipID].victimIndex+1
        v = ABT_InfBeam[shipID].victimIndex
        ABT_InfBeam[shipID].victims[v]={}
        ABT_InfBeam[shipID].victims[v].sob = newVictim.group
        ABT_InfBeam[shipID].victims[v].stage = IBEAM_VICTIMSTAGE_WAITINGBEAM
        local a = MFR_SafeVolName()
        local b = MFR_SafeVolName()
        Volume_AddSphere(a,SobGroup_GetPosition(CustomGroup),0.1)
        Volume_AddSphere(b,SobGroup_GetPosition(newVictim.group),0.1)
        ABT_InfBeam[shipID].victims[v].beamStart = a
        ABT_InfBeam[shipID].victims[v].beamEnd = b
        ABT_InfBeam[shipID].victims[v].infectionTime = Universe_GameTime()
        ABT_InfBeam[shipID].victims[v].type = newVictim.type
        --ABT_InfBeam[shipID].victims[v].juiceRemaining
        ABT_InfBeam[shipID].juiceLeft = ABT_InfBeam[shipID].juiceLeft-newVictim.cost
        InfBeam_InfectionBeginEffects(newVictim.group,playerIndex)
        return 1
    end
end
function InfBeam_InfectionBeginEffects(sob,playerIndex)
    SobGroup_LeaveStrikeGroup((sob),SobGroup_OwnedBy(sob))
    SobGroup_MakeSelectable(sob,0)
    --SobGroup_Disable(sob,1)
    SobGroup_Stop(SobGroup_OwnedBy(sob),sob)
     --Not sure if this has any real impact but I'm using it to flag the ship as being infected
    SobGroup_AbilityActivate(sob,AB_Lights,0)
end
function InfBeam_InfectionEndEffects(sob,playerIndex)
    SobGroup_SetSpeed(sob,1)
    SobGroup_MakeSelectable(sob,1)
    SobGroup_AbilityActivate(sob,AB_Lights,1)
end

function InfBeam_FindJumpTarget(CustomGroup, playerIndex, shipID,fRange,sTargetGroup)
    if(ABT_InfBeam[shipID].juiceLeft <= 0) then
        return nil
    end
    local i = 0
    local allEnemies = MFR_CreateSafeSob()
    local candidateSob = MFR_CreateSafeSob()
    while i < Universe_PlayerCount() do
        if(i == playerIndex ) then
        elseif(AreAllied(i,playerIndex)==1) then
        else
            SobGroup_SobGroupAdd(allEnemies,"Player_Ships"..i)
        end
        i=i+1
    end
    if( sTargetGroup ~= nil) then
        SobGroup_FillProximitySobGroup(candidateSob,sTargetGroup,CustomGroup,fRange)
    else
        SobGroup_FillProximitySobGroup(candidateSob,allEnemies,CustomGroup,fRange)
    end
    InfBeam_SubtractVictims(candidateSob,candidateSob,shipID)
    if SobGroup_Count(candidateSob)<1 then
        return nil
    end
    candidateTable = MFR_SobGroup_SplitByFilter(candidateSob,AB_InfBeam_SplitMethod)
    local closestGroup = 0
    local closestDistance = nil
    local closestCost = 0
    local closestCategory = ""
    local distance
    for k,v in candidateTable do
        if(AB_InfBeam_CostTable[k] ~= nil and AB_InfBeam_CostTable[k] <= ABT_InfBeam[shipID].juiceLeft) then
            for k2,v2 in v do
                distance = SobGroup_GetDistanceToSobGroup(v2,CustomGroup)
                --We have to make sure the target isn't being infected, as double infecting something
                -- would cause a crash if possible
                if closestDistance == nil and SobGroup_CanDoAbility(v2,AB_Lights)==1 then
                    closestDistance = distance
                    closestGroup = v2
                    closestCost = AB_InfBeam_CostTable[k]
                    closestCategory= k
                elseif closestDistance == nil then
                elseif distance < closestDistance and SobGroup_CanDoAbility(v2,AB_Lights)==1 then
                    closestDistance = distance
                    closestGroup = v2
                    closestCost = AB_InfBeam_CostTable[k]
                    closestCategory= k
                end
            end
        end
    end
    if closestDistance > fRange then
        return nil
    end
    local retTable ={}
    if(closestGroup ~= nil) then
        retTable.group = closestGroup
        retTable.type = closestCategory
        retTable.cost = closestCost
        return retTable
    end
    return nil
end
function InfBeam_SubtractVictims(dest,source,shipID)
    for k,v in ABT_InfBeam[shipID].victims do
        SobGroup_FillSubstract(dest,source,v.sob)
    end
end
function InfBeam_UpdateBeams(shipID)
    --Check the beams table and remove any that are over their age
    local time = Universe_GameTime()
    for k,v in ABT_InfBeam[shipID].beams do
        if v + AB_InfBeam_BeamLifetime < time then
            FX_StopEffect(k)
            ABT_InfBeam[shipID].beams[k]=nil
        end
    end
end

--Run periodicly while the ship is alive
function OnUpdate_InfBeam(CustomGroup, playerIndex, shipID)
    local time = Universe_GameTime()
    for k,v in ABT_InfBeam[shipID].victims do
        InfBeam_HandleVictim(CustomGroup, playerIndex, shipID,v,time)
    end
    InfBeam_UpdateBeams(shipID)
    if(ABT_InfBeam[shipID].lastFireTime~= nil and SobGroup_CanDoAbility(CustomGroup,AB_Custom)==0) then
        if(ABT_InfBeam[shipID].rechargeTime+ABT_InfBeam[shipID].lastFireTime<= time) then
            SobGroup_AbilityActivate(CustomGroup,AB_Custom,1)
        end
    end
end

function InfBeam_HandleVictim(CustomGroup, playerIndex, shipID,v,time)
    if(v.stage == IBEAM_VICTIMSTAGE_WAITINGBEAM) then
        SobGroup_Stop(SobGroup_OwnedBy(v.sob),v.sob)
        beam_id = FX_PlayEffectBetweenPoints("beam_generic_kus", v.beamStart, v.beamEnd, 10)
        ABT_InfBeam[shipID].beams[beam_id]=time
        v.stage = IBEAM_VICTIMSTAGE_PREJUMP
    end
    if(v.stage ==IBEAM_VICTIMSTAGE_PREJUMP and v.infectionTime+AB_InfBeam_PreJumpDelay <= time) then
        SobGroup_Stop(SobGroup_OwnedBy(v.sob),v.sob)
        if(ABT_InfBeam[shipID].juiceLeft > 0) then
            InfBeam_Jump(v.sob,playerIndex,shipID,AB_InfBeam_ChainJumpRange)
        end
        v.stage = IBEAM_VICTIMSTAGE_POSTJUMP
    end
    if(v.stage ==IBEAM_VICTIMSTAGE_POSTJUMP and v.infectionTime+AB_InfBeam_PreJumpDelay+AB_InfBeam_PostJumpDelay <= time) then
        local tempVol = MFR_SafeVolName()
        Volume_AddSphere(tempVol,SobGroup_GetPosition(v.sob),1)
        SobGroup_SwitchOwner(v.sob,playerIndex)
        SobGroup_FillSobGroupInVolume(v.sob,"Player_Ships"..playerIndex,tempVol)
        v.stage = IBEAM_VICTIMSTAGE_DONE
    elseif(v.stage ~= IBEAM_VICTIMSTAGE_DONE) then
        SobGroup_Stop(SobGroup_OwnedBy(v.sob),v.sob)
        SobGroup_SetSpeed(v.sob,0.25)
        tumbleScale = 0.25
        x = 1
        y = 1
        z = 1
        SobGroup_Tumble(v.sob,{x*tumbleScale,y*tumbleScale,z*tumbleScale})
    else
        InfBeam_InfectionEndEffects(v.sob,playerIndex)
    end
end

--Run when the ship die
function OnDestroy_InfBeam(CustomGroup, playerIndex, shipID)
    for k,v in ABT_InfBeam[shipID].victims do
        InfBeam_InfectionEndEffects(v.sob,playerIndex)
        SobGroup_SwitchOwner(v.sob,playerIndex)
    end
    for k,v in ABT_InfBeam[shipID].beams do
        FX_StopEffect(k)
    end
    ABT_InfBeam[shipID] = nil
end

