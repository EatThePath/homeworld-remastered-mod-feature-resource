if (LIBEXT_LUA == nil) then
	LIBEXT_LUA = [[LIBEXT_LUA]]

	true = (nil == nil)
	false = (nil ~= nil)

	function isnil(x)
		return (type(x) == "nil")
	end

	function isnumber(x)
		return (type(x) == "number")
	end

	function isnan(x)
		return (isnumber(x) and (x ~= x))
	end

	function isstring(x)
		return (type(x) == "string")
	end

	function istable(x)
		return (type(x) == "table")
	end

	function isfunction(x)
		return (type(x) == "function")
	end

	function isuserdata(x)
		return (type(x) == "userdata")
	end

	function tobool(x)
		return ((x ~= "false") and (x ~= "0") and (x ~= 0) and (not isnil(x)));
	end

	function require(Library)
		local Path = format("data:scripts/%s", tostring(Library))

		if (isnil(strfind(Library, ".", 1, true))) then
			Path = format("%s.lua", Path);
		end

		dofilepath(Path);
	end

	doscanpath("data:scripts/libext", "*.lua");
end