-- Sobgroup Wrappers
-- Boolean wrappers for SobGroup functions that return booleans as integers

local _G = globals();

rawset(_G, "SobGroup_AreAllInHyperspace", function(SobName)
	return tobool(%SobGroup_AreAllInHyperspace(SobName));
end);

rawset(_G, "SobGroup_CanDoAbility", function(SobName, Ability)
	return tobool(%SobGroup_CanDoAbility(SobName, Ability));
end);

rawset(_G, "SobGroup_IsDoingAbility", function(SobName, Ability)
	return tobool(%SobGroup_IsDoingAbility(SobName, Ability));
end);

rawset(_G, "SobGroup_Exists", function(SobName)
	assertf(isstring(SobName), "Object \"%s\" is not a string!", type(SobName));
	return tobool(%SobGroup_Exists(SobName));
end);

rawset(_G, "SobGroup_Empty", function(SobName)
	assertf(isstring(SobName), "Object \"%s\" is not a string!", type(SobName));
	return (not SobGroup_Exists(SobName)) or tobool(%SobGroup_Empty(SobName));
end);
 
rawset(_G, "SobGroup_AreAnyOfTheseTypes", function(SobName, Type)
	assertf(isstring(Type), "Object \"%s\" is not a string!", type(Type));
	assertf(isstring(SobName), "Object \"%s\" is not a string!", type(SobName));
	return tobool(%SobGroup_AreAnyOfTheseTypes(SobName, Type));
end);

rawset(_G, "SobGroup_IsDockedSobGroup", function(SobName, DockSobName)
	assertf(isstring(SobName), "Object \"%s\" is not a string!", type(SobName));
	assertf(isstring(DockSobName), "Object \"%s\" is not a string!", type(DockSobName));
	return tobool(%SobGroup_IsDockedSobGroup(SobName, DockSobName));
end);

rawset(_G, "SobGroup_IsCloaked", function(SobName)
	assertf(isstring(SobName), "Object \"%s\" is not a string!", type(SobName));
	return tobool(%SobGroup_IsCloaked(SobName));
end);

rawset(_G, "SobGroup_PlayerIsInSensorRange", function(sSobGroupName, iPlayerID)
	assertf(isnumber(iPlayerID), "Object \"%s\" is not a number!", type(iPlayerID));
	assertf(isstring(sSobGroupName), "Object \"%s\" is not a string!", type(sSobGroupName));
	return tobool(%SobGroup_PlayerIsInSensorRange(sSobGroupName, iPlayerID));
end)

rawset(_G, "SobGroup_GroupInGroup", function(sSobGroupName, sSobGroupNameToFind)
	assertf(isstring(sSobGroupName), "Object \"%s\" is not a string!", type(sSobGroupName));
	assertf(isstring(sSobGroupNameToFind), "Object \"%s\" is not a string!", type(sSobGroupNameToFind));
	return tobool(%SobGroup_GroupInGroup(sSobGroupName, sSobGroupNameToFind));
end)

rawset(_G, "SobGroup_AreAnySquadronsInsideDustCloud", function(sSobGroupName, sDustCloudName)
	assertf(isstring(sSobGroupName), "Object \"%s\" is not a string!", type(sSobGroupName));
	assertf(isstring(sDustCloudName), "Object \"%s\" is not a string!", type(sDustCloudName));
	return tobool(%SobGroup_AreAnySquadronsInsideDustCloud(sSobGroupName, sDustCloudName));
end)

rawset(_G, "SobGroup_AreAnySquadronsOutsideDustCloud", function(sSobGroupName, sDustCloudName)
	assertf(isstring(sSobGroupName), "Object \"%s\" is not a string!", type(sSobGroupName));
	assertf(isstring(sDustCloudName), "Object \"%s\" is not a string!", type(sDustCloudName));
	return tobool(%SobGroup_AreAnySquadronsOutsideDustCloud(sSobGroupName, sDustCloudName));
end)