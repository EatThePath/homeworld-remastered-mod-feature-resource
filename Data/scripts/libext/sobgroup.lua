-- Sobgroup
-- Library of additional sobgroup utilities

dofilepath("data:scripts/familylist.lua");

local SobGroup =
{
	Regfile =
	{
		Next = -1, -- Gets set to 0 on first call to SobGroup_Temp
		Count = 31,
		SGUID = 0,
	},

	Stack =
	{
		StackIndex = -1,
	},

	Families =
	{
		["buildFamily"] = true,
		["displayFamily"] = true,
		["attackFamily"] = true,
		["dockFamily"] = true,
		["avoidanceFamily"] = true,
		["collisionFamily"] = true,
		["collisionDamageFamily"] = true,
		["autoFormationFamily"] = true,
		["armourFamily"] = true,
		["unitcapsFamily"] = true,
	},
};

do
	for reg = 0, SobGroup.Regfile.Count do
		local sgr = format("sgr%i", reg);
		SobGroup_Create(sgr);
		SobGroup_Clear(sgr);
		SobGroup.Regfile[reg] = sgr;
	end

	local sbp = format("ssp%i", SobGroup.Stack.StackIndex);
	SobGroup_Create(sbp);
	SobGroup_Clear(sbp);
end

-- SobGroup_Temp
-- Allocates a sobgroup register in round robin order from 32 registers
-- sgr and idx can be specified to create custom sobgroup temporaries that are initialized for you
function SobGroup_Temp(sgr, idx)
	local Regfile = %SobGroup.Regfile;
	if isnil(sgr) then
		Regfile.Next = Regfile.Next + 1;

		if (Regfile.Next > Regfile.Count) then
			Regfile.Next = 0;
		end

		sgr = Regfile[Regfile.Next];
	else
		sgr = sgr..tostring((isnil(idx) and Regfile.SGUID) or idx);
		Regfile.SGUID = ((isnil(idx) and Regfile.SGUID + 1) or Regfile.SGUID);
	end

	SobGroup_Initialize(sgr);
	return sgr;
end

-- SobGroup_Push
-- Adds the specified sobgroup contents to the contents on the stack and pushes that as a new sobgroup
-- Subtracts the specified sobgroup from the top-of-stack contents if SubtractMode evaluates to true
function SobGroup_Push(SG, SubtractMode)
	local sbp = format("ssp%i", %SobGroup.Stack.StackIndex);
	%SobGroup.Stack.StackIndex = %SobGroup.Stack.StackIndex + 1;
	local ssp = format("ssp%i", %SobGroup.Stack.StackIndex);
	SobGroup_Initialize(ssp);
	SobGroup_SobGroupAdd(ssp, sbp);
	local Operation = (tobool(SubtractMode) and SobGroup_Subtract) or SobGroup_SobGroupAdd;
	Operation(ssp, SG);
	return ssp;
end

-- SobGroup_Pop
-- Pops the current sobgroup from the top of the stack and returns the new top of the stack
-- Held references to popped sobgroups are invalid
function SobGroup_Pop()
	local StackIndex = %SobGroup.Stack.StackIndex;
	local ssp = format("ssp%i", -1);
	if StackIndex > 0 then
		ssp = format("ssp%i", StackIndex);
		SobGroup_Clear(ssp);
		%SobGroup.Stack.StackIndex = StackIndex - 1;
	elseif StackIndex == 0 then
		ssp = format("ssp%i", StackIndex);
		SobGroup_Clear(ssp);

		%SobGroup.Stack.StackIndex = StackIndex - 1;
		local sbp = format("ssp%i", StackIndex);
		SobGroup_Clear(sbp);
	end

	return ssp;
end

function SobGroup_Initialize(SobName)
	SobGroup_CreateIfNotExist(SobName);
	SobGroup_Clear(SobName);

	return SobName;
end

function SobGroup_Equals(A, B)
	local SGTemp = SobGroup_Temp();
	SobGroup_FillCompare(SGTemp, A, B);
	return (SobGroup_Count(A) == SobGroup_Count(B) == SobGroup_Count(SGTemp));
end

function SobGroup_MoveContents(Destination, Source)
	SobGroup_Clear(Destination);
	SobGroup_SobGroupAdd(Destination, Source);
	SobGroup_Clear(Source);
end

function SobGroup_Subtract(Minuend, Subtrahend)
	local SGTemp = SobGroup_Temp();
	SobGroup_FillSubstract(SGTemp, Minuend, Subtrahend);
	SobGroup_Clear(Minuend);
	SobGroup_SobGroupAdd(Minuend, SGTemp);
end

function SobGroup_FilterMove(Destination, Source, Key, Value)
	local SGTemp = SobGroup_Initialize("sgr:filtermove");
	SobGroup_FilterInclude(SGTemp, Source, Key, Value);
	if (SobGroup_Populated(SGTemp)) then
		SobGroup_Subtract(Source, SGTemp);
		SobGroup_SobGroupAdd(Destination, SGTemp);
	end
end

function SobGroup_FilterDockedShips(SGIn)
	local SGTemp, SGDocked = SobGroup_Initialize("sgr:filterdocked"), SobGroup_Initialize("sgr:filterdocked2");
	SobGroup_GetSobGroupDockedWithGroup(SGIn, SGDocked);
	SobGroup_FillCompare(SGTemp, SGIn, SGDocked);
	SobGroup_Subtract(SGIn, SGTemp);
	return SGTemp;
end

function SobGroup_FilterCloakedShips(SGIn)
	local SGTemp = SobGroup_Initialize("sgr:filtercloaked");
	local Ships = SobGroup_Split(SGIn);
	for _, Ship in Ships do
		if SobGroup_IsCloaked(Ship) and SobGroup_IsDoingAbility(Ship, AB_Cloak) then
			SobGroup_SobGroupAdd(SGTemp, Ship);
			SobGroup_Subtract(SGIn, Ship);
		end
	end

	return SGTemp;
end

function SobGroup_FilterDustCloudedShips(SGIn)
	local SGTemp = SobGroup_Initialize("sgr:filterdustcloud");
	local Ships = SobGroup_Split(SGIn);
	for _, Ship in Ships do
		if SobGroup_IsInDustCloud(Ship) then
			SobGroup_SobGroupAdd(SGTemp, Ship);
			SobGroup_Subtract(SGIn, Ship);
		end
	end

	return SGTemp;
end

function SobGroup_FilterNebulaCloudedShips(SGIn)
	local SGTemp = SobGroup_Initialize("sgr:filterdustcloud");
	local Ships = SobGroup_Split(SGIn);
	for _, Ship in Ships do
		if SobGroup_IsInNebula(Ship) then
			SobGroup_SobGroupAdd(SGTemp, Ship);
			SobGroup_Subtract(SGIn, Ship);
		end
	end

	return SGTemp;
end

function SobGroup_FilterHyperspacingShips(SGIn)
	local SGTemp = SobGroup_Initialize("sgr:filterhs");
	local Ships = SobGroup_Split(SGIn);
	for _, Ship in Ships do
		if SobGroup_AreAllInHyperspace(Ship) and (SobGroup_IsDoingAbility(Ship, AB_Hyperspace) or SobGroup_IsDoingAbility(Ship, AB_HyperspaceViaGate)) then
			SobGroup_SobGroupAdd(SGTemp, Ship);
			SobGroup_Subtract(SGIn, Ship);
		end
	end

	return SGTemp;
end

function SobGroup_HasDockedShips(SGIn)
	local SGTemp = SobGroup_Temp();
	SobGroup_GetSobGroupDockedWithGroup(SGIn, SGTemp);
	return SobGroup_Populated(SGTemp);
end

function SobGroup_Populated(SG)
	return tobool(SobGroup_Exists(SG)) and (not tobool(SobGroup_Empty(SG)));
end

function SobGroup_IsSquadron(SGIn)
	local SGTemp = SobGroup_Temp();
	SobGroup_FilterInclude(SGTemp, SGIn, "avoidanceFamily", "Strikecraft");
	SobGroup_FilterDockedShips(SGTemp);
	return tobool(SobGroup_Populated(SGTemp) and (SobGroup_Count(SGTemp) > 1));
end

function SobGroup_SpawnDebugProbe(SGIn, Type)
	local SGTemp = SobGroup_Temp();
	if not isstring(Type) then
		Type = "Hgn_TargetDrone";
	end

	Player_FillProximitySobGroup(SGTemp, 0, SGIn, 1000);
	if not SobGroup_AreAnyOfTheseTypes(SGTemp, Type) then
		SobGroup_Clear(SGTemp);
		local VolumeName = "SobGroup_SpawnDebugProbe";
		Volume_AddSphere(VolumeName, SobGroup_GetPosition(SGIn), 0);
		SobGroup_SpawnNewShipInSobGroup(0, Type, "SobGroup_SpawnDebugProbe", SGTemp, VolumeName);
		Volume_Delete(VolumeName);
	end

	SobGroup_SetAsDeployed(SGTemp);
	SobGroup_SetInvulnerability(SGTemp, 1);
	return SGTemp;
end

function SobGroup_TeleportToSobGroup(SGIn, SGTarget)
	SobGroup_Despawn(SGIn);
	SobGroup_Spawn(SGIn, SobGroup_PositionToVolume(SGTarget, 0));
end

function SobGroup_SplitByFamily(SGIn, ChosenFamily)
	local SGOut, Families, _G = {}, %SobGroup.Families, globals();
	local Index = 0;

	ChosenFamily = tostring(ChosenFamily);
	assertf(Families[ChosenFamily], "[SobGroup_SplitByFamily] Invalid family \"%s\" specified", ChosenFamily);

	for Family, _ in Families do
		for _, Entry in _G[Family] do
			local SGTemp = SobGroup_Temp("sgsr", Index); Index = Index + 1;
			SobGroup_FilterInclude(SGTemp, SGIn, Family, Entry.name);
			if SobGroup_Populated(SGTemp) then
				SGOut[Family] = SGTemp;
			end
		end
	end

	return SGOut;
end

function SobGroup_Split(SGIn, IncludeDocked, SGTOut, Index)
	if not isnumber(Index) then Index = 0; end
	if not istable(SGTOut) then SGTOut = {}; end

	local SGSet, SGDocked = SobGroup_Initialize("sgsr:set"), SobGroup_Initialize("sgsr:docked");
	SobGroup_GetSobGroupDockedWithGroup(SGIn, SGDocked);

	SobGroup_SobGroupAdd(SGSet, SGIn);
	while SobGroup_Populated(SGSet) do
		local SGTemp = SobGroup_Temp("sgsr", Index); Index = Index + 1;
		SobGroup_FillShipsByIndexRange(SGTemp, SGSet, 0, 1);
		SobGroup_Subtract(SGSet, SGTemp);
		tinsert(SGTOut, SGTemp);
	end

	if IncludeDocked and SobGroup_Populated(SGDocked) then
		SobGroup_Split(SGDocked, IncludeDocked, SGTOut, Index);
	end

	SGTOut["n"] = nil;
	return SGTOut;
end

function SobGroup_CanMove(SG)
	return SobGroup_Exists(SG) and SobGroup_CanDoAbility(SG, AB_Move);
end

function SobGroup_Distance(SG1, SG2)
	local Distance = 1.0 / 0.0; -- A sobgroup that doesn't exist is infinitely far away

	if SobGroup_Populated(SG1) and SobGroup_Populated(SG2) then
		local P1, P2 = SobGroup_GetPosition(SG1), SobGroup_GetPosition(SG2);
		Distance = sqrt(pow(P1[1] - P2[1], 2.0) + pow(P1[2] - P2[2], 2.0) + pow(P1[3] - P2[3], 2.0));
	end

	return Distance;
end

function SobGroup_IsClosestOfType(SobName, SobTarget)
	local Result, SGTemp, Distance = true, SobGroup_Temp(), SobGroup_Distance(SobName, SobTarget);
	Player_FillShipsByType(SGTemp, SobGroup_GetPlayerOwner(SobName), SobGroup_GetShipType(SobName));
	SobGroup_Subtract(SGTemp, SobName);
	for _, Ship in SobGroup_Split(SGTemp, false, {}, -32768) do
		if SobGroup_Distance(Ship, SobTarget) < Distance then
			Result = false;
			break;
		end
	end

	return Result;
end

function SobGroup_PositionToVolume(SG, Radius)
	return Volume_Temp(SobGroup_GetPosition(SG), Radius or 0.0);
end

function SobGroup_AllianceIsInSensorRange(SG, PlayerIndex)
	local IsInRange = false;

	for Player = 0, Universe_PlayerCount() - 1 do
		if (Player == PlayerIndex) or (AreAllied(Player, PlayerIndex) == 1) then
			IsInRange = IsInRange or SobGroup_PlayerIsInSensorRange(SG, Player);
			if IsInRange then
				break;
			end
		end
	end

	return IsInRange;
end

function SobGroup_IsInDustCloud(SG)
	local IsInDustCloud = false;

	for Name, CloudObjs in Level_GetDustClouds() do
		if SobGroup_AreAnySquadronsInsideDustCloud(SG, Name) then
			IsInDustCloud = true;
			break;
		end
	end

	return IsInDustCloud;
end

function SobGroup_IsInNebula(SG)
	local IsInNebula = false;

	for Name, Nebula in Level_GetNebulae() do
		if SobGroup_AreAnySquadronsInsideNebula(SG, Name) then
			IsInNebula = true;
			break;
		end
	end

	return IsInNebula;
end

function SobGroup_FilterShips(SG, FilterFunction, ...)
	
end