-- Ship Parser
-- A facility for retrieving ship data at runtime
-- Unlike SobGroup_GetStaticF this is only capable of returning the initial default values
-- Also unlike GetStaticF this works for any tunable

local ShipCache = {};

function addAbility(NewShipType, sAbilityName, ...)
	if NewShipType.addAbility == nil then
		NewShipType.addAbility = {};
	end
	
	NewShipType.addAbility[sAbilityName] = arg;
end

function addCustomCode(NewShipType, ...)
	NewShipType.addCustomCode = arg;
end

function addMagneticField(NewShipType, ...)
	NewShipType.addMagneticField = arg;
end

function addShield(NewShipType, sShieldType, ...)
	if NewShipType.addShield == nil then
		NewShipType.addShield = {};
	end

	NewShipType.addShield[sShieldType] = arg;
end

function AddShipAbility(NewShipType, sString, ...)
	if NewShipType.AddShipAbility == nil then
		NewShipType.AddShipAbility = {};
	end

	NewShipType.AddShipAbility[sString] = arg;
end

function AddShipMultiplier(NewShipType, sMultiplierType)
	if NewShipType.AddShipMultiplier == nil then
		NewShipType.AddShipMultiplier = {};
	end

	NewShipType.AddShipMultiplier[sMultiplierType] = arg;
end

function getRulesNum(NewShipType, _, DefaultValue)
	return DefaultValue;
end

function getRulesStr(NewShipType, _, DefaultValue)
	return DefaultValue;
end

function getShipNum(NewShipType, _, DefaultValue)
	return DefaultValue;
end

function getShipStr(NewShipType, _, DefaultValue)
	return DefaultValue;
end

function loadLatchPointList(NewShipType, Type, ...)
	if isnil(NewShipType.loadLatchPointList) then
		NewShipType.loadLatchPointList = {};
	end

	NewShipType.loadLatchPointList[Type] = arg;
end

function LoadModel(NewShipType, bEnable)
	NewShipType.LoadModel = bEnable;
end

function LoadSharedModel(NewShipType, Type)
	NewShipType.LoadSharedModel = Type;
end

function loadShipPatchList(NewShipType, ...)
	if NewShipType.loadShipPatchList == nil then
		NewShipType.loadShipPatchList = {};
	end

	NewShipType.loadShipPatchList = arg;
end

function setCollisionDamageFromModifier(NewShipType, Family, Modifier)
	if NewShipType.setCollisionDamageFromModifier == nil then
		NewShipType.setCollisionDamageFromModifier = {};
	end

	NewShipType.setCollisionDamageFromModifier[Family] = Modifier;
end

function setCollisionDamageToModifier(NewShipType, Family, Modifier)
	if NewShipType.setCollisionDamageToModifier == nil then
		NewShipType.setCollisionDamageToModifier = {};
	end

	NewShipType.setCollisionDamageToModifier[Family] = Modifier;
end

function setConcurrentBuildLimit(NewShipType, Type, Limit)
	if NewShipType.setConcurrentBuildLimit == nil then
		NewShipType.setConcurrentBuildLimit = {};
	end

	NewShipType.setConcurrentBuildLimit[Type] = Limit;
end

function setEngineBurn(NewShipType, ...)
	if NewShipType.setEngineBurn == nil then
		NewShipType.setEngineBurn = {};
	end

	tinsert(NewShipType.setEngineBurn, arg);
end

function setEngineGlow(NewShipType, ...)
	if isnil(NewShipType.setEngineGlow) then
		NewShipType.setEngineGlow = {};
	end

	tinsert(NewShipType.setEngineGlow, arg);
end

function setEngineTrail(NewShipType, iIndex, ...)
	if isnil(NewShipType.setEngineTrail) then
		NewShipType.setEngineTrail = {};
	end

	NewShipType.setEngineTrail[iIndex] = arg;
end

function setShaderChannels(NewShipType, ...)
	NewShipType.setShaderChannels = arg;
end

function setSpecialDieTime(NewShipType, Family, Value)
	if isnil(NewShipType.setSpecialDieTime) then
		NewShipType.setSpecialDieTime = {};
	end

	NewShipType.setSpecialDieTime[Family] = Value;
end

function setSpeedvsAccuracyApplied(NewShipType, ...)
	NewShipType.setSpeedvsAccuracyApplied = arg;
end

function setSupplyValue(NewShipType, Family, Value)
	if isnil(NewShipType.setSupplyValue) then
		NewShipType.setSupplyValue = {};
	end

	NewShipType.setSupplyValue[Family] = Value;
end

function setTacticsMults(NewShipType, Parameter, ...)
	if isnil(NewShipType.setTacticsMults) then
		NewShipType.setTacticsMults = {};
	end

	NewShipType.setTacticsMults[Parameter] = arg;
end

function setTargetBox(NewShipType, iIndex, ...)
	if isnil(NewShipType.setTargetBox) then
		NewShipType.setTargetBox = {};
	end

	NewShipType.setTargetBox[iIndex] = arg;
end

function SpawnDustCloudOnDeath(NewShipType, ...)
	if isnil(NewShipType.SpawnDustCloudOnDeath) then
		NewShipType.SpawnDustCloudOnDeath = {};
	end

	tinsert(NewShipType.SpawnDustCloudOnDeath, arg);
end

function SpawnSalvageOnDeath(NewShipType, ...)
	if isnil(NewShipType.SpawnSalvageOnDeath) then
		NewShipType.SpawnSalvageOnDeath = {};
	end

	tinsert(NewShipType.SpawnSalvageOnDeath, arg);
end

function StartShipConfig()
	return {};
end

function StartShipHardPointConfig(NewShipType, ...)
	if isnil(NewShipType.StartShipHardPointConfig) then
		NewShipType.StartShipHardPointConfig = {};
	end

	tinsert(NewShipType.StartShipHardPointConfig, arg);
end

function StartShipWeaponConfig(NewShipType, ...)
	if isnil(NewShipType.StartShipWeaponConfig) then
		NewShipType.StartShipWeaponConfig = {};
	end

	tinsert(NewShipType.StartShipWeaponConfig, arg);
end

function Sob_GetShipInfo(ShipType)
	local Ship = %ShipCache[ShipType];

	if not istable(Ship) then
		ShipType = strlower(ShipType);

		call(dofilepath, {
			format("data:ship/%s/%s.ship", ShipType, ShipType);
		}, "x");

		%ShipCache[ShipType] = NewShipType;
		Ship = %ShipCache[ShipType];

		setglobal("NewShipType", nil);
	end

	return Ship;
end