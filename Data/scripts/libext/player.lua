-- Playerlib
-- Extra functions for player specific operations

function Player_RUTransaction(PlayerID, Amount)
	local Balance = Player_GetRU(PlayerID);
	local NewBalance = Amount + Balance;
	Player_SetRU(PlayerID, NewBalance);
	return NewBalance, Balance;
end

function Player_GetNumberOfShips(PlayerIndex, ShipTypeOrSobGroup)
	if SobGroup_Exists(ShipTypeOrSobGroup) then
		ShipTypeOrSobGroup = SobGroup_GetShipType(ShipTypeOrSobGroup);
	end

	return SobGroup_Count(ShipTypeOrSobGroup);
end

function Player_HasMultipleOfShipType(PlayerIndex, ShipTypeOrSobGroup)
	return Player_GetNumberOfShips(PlayerIndex, ShipTypeOrSobGroup) > 1;
end