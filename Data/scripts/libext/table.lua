-- Table
-- Adds some additional math functions not defined by Lua 4.0

function tcount(Table)
	local Count = 0;

	for _, _ in Table do
		Count = Count + 1;
	end

	return Count;
end

function tclear(Table)
	local Keys = {};
	for Key, _ in Table do
		tinsert(Keys, Key);
	end

	for _, Key in Keys do
		Table[Key] = nil;
	end
end

function ttostring(Table)
	return strsub(tostring(Table), 8);
end

function tdesc(Table, String, Indent)
	String = String or "";
	Indent = Indent or 0;

	local Tabs = strrep('\t', Indent);
	for Key, Value in Table do
		if istable(Value) then
			String = tdesc(Table, String, Indent + 1);
		elseif isnumber(Value) then
			String = format("%s%s%s:\t%f", Tabs, String, Key, Value);
		elseif isstring(Value) then
			String = format("%s%s%s:\t%s", Tabs, String, Key, Value);
		end
	end

	return String;
end

function tclone(Table)
	local Clone = {};
	for Idx, Val in Table do
		Clone[Idex] = Val;
	end

	return Clone;
end

function pairs(Table)
	local Pairs = {};
	for Idx, Val in Table do
		Pairs[Idx] = Val;
	end

	Pairs["n"] = nil;
	return Pairs;
end