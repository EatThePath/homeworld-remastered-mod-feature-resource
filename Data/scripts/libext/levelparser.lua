-- LevelParser
-- A facility for retrieving level data at runtime

local LevelName = format("%s.level", getCurrentLevelName());
local LevelData = {
	Points = {},
	Nebulae = {},
	Asteroids = {},
	DustClouds = {},
	WorldBoundsInner = nil,
	WorldBoundsOuter = nil,
}

local Paths = {
	"data:leveldata/multiplayer/deathmatch",
	"data:leveldata/multiplayer/deathmatchhw1",
	"data:leveldata/multiplayer/deathmatchhw2",
}

local Next = nil;
repeat
	Next = next(Paths, Next);
	doscanpath(Paths[Next], LevelName);
until isfunction(DetermChunk) or isnil(Next);

function addPoint(sPointName, tPosition, tRotation)
	tinsert(%LevelData.Points, {
		Name = sPointName,
		Position = tPosition,
		Rotation = tRotation,
	});
end

function addAsteroid(sAsteroidType, tPosition, fRU, ...)
	tinsert(%LevelData.Asteroids, {
		Type = sAsteroidType,
		Position = tPosition,
		YieldModifier = fRU,
		UnknownParams = arg,
	});
end

function addDustCloud(sObjName, sDustCloudType, tPosition, tColor, fUnknown, fSize)
	if not istable(%LevelData.DustClouds[sObjName]) then
		%LevelData.DustClouds[sObjName] = {};
	end

	tinsert(%LevelData.DustClouds[sObjName], {
		Type = sDustCloudType,
		Position = tPosition,
		Color = tColor,
		UnknownValue = fUnknown,
		Radius = fSize,
	});
end

function addNebula(sNebulaName, sNebulaType, tPosition, tColor, fUnknown, fSize)
	if not istable(%LevelData.Nebulae[sNebulaName]) then
		%LevelData.Nebulae[sNebulaName] = {};
	end

	tinsert(%LevelData.Nebulae[sNebulaName], {
		Type = sNebulaType,
		Position = tPosition,
		Color = tColor,
		UnknownValue = fUnknown,
		Radius = fSize,
	});
end

function setWorldBoundsInner(tPosition, tDimensions)
	%LevelData.WorldBoundsInner = {
		Origin = tPosition,
		Dimensions = tDimensions,
	}
end

function setWorldBoundsOuter(tPosition, tDimensions)
	%LevelData.WorldBoundsOuter = {
		Origin = tPosition,
		Dimensions = tDimensions,
	}
end

function addSquadron()
	
end

call(DetermChunk, {}, "x");

function Level_GetPoints()
	return %LevelData["Points"];
end

function Level_GetAsteroids()
	return %LevelData["Asteroids"];
end

function Level_GetDustClouds()
	return %LevelData["DustClouds"];
end

function Level_GetNebulae()
	return %LevelData["DustClouds"];
end