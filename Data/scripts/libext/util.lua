-- Util
-- Adds some additional utilities not supplied by Lua 4.0

function printf(FormatStr, ...)
	tinsert(arg, 1, FormatStr)
	local Result = call(format, arg, "x")
	if isstring(Result) then
		print(Result)
	end
end

function tprintf(FormatStr, ...)
	FormatStr = format("%s %s", "[%4.6f:%4.6f]", FormatStr)
	tinsert(arg, 1, FormatStr)
	tinsert(arg, 2, Universe_GameTime())
	tinsert(arg, 3, clock())
	call(printf, arg, "x")
end

function assertf(Condition, FormatStr, ...)
	if not Condition then
		tinsert(arg, 1, FormatStr)
		local Result = call(format, arg, "x")
		assert(Condition, (isstring(Result) and Result) or "")
	end
end

function assertobj(CondFunc, Object, FormatStr, ...)
	if not CondFunc(Object) then
		tinsert(arg, 1, FormatStr)
		local Result = call(format, arg, "x")
		assert(false, (isstring(Result) and Result) or "")
	end
end