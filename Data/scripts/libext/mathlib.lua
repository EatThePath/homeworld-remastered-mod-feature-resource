-- Mathlib
-- Adds some additional math functions not defined by Lua 4.0

function lerp(x, y, t)
	return (((1.0 - t) * x) + (t * y))
end

function pow(x, y)
	return x^y
end

function distance(x, y)
	return sqrt(pow(x[1] - y[1], 2.0) + pow(x[2] - y[2], 2.0) + pow(x[3] - y[3], 2.0))
end