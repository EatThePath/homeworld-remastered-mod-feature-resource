-- Universe
-- Library of additional universe utilities

function Universe_IsShuttingDown()
	local Player = %Universe_CurrentPlayer();
	local Alive = %Player_IsAlive(Player);
	local PlayerShips = %format("Player_Ships%i", Player);
	return tobool((Alive == 1) and (SobGroup_Count(PlayerShips) == 0));
end