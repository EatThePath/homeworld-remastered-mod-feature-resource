-- Volume
-- Library of additional volume utilities

local Volume =
{
	Regfile =
	{
		Next = -1, -- Gets set to 0 on first call to Volume_Temp
		Count = 31,
		VMUID = 0,
	},
};

for reg = 0, Volume.Regfile.Count do
	local vmr = "vmr"..tostring(reg);
	Volume.Regfile[reg] = vmr;
end

function Volume_Temp(Pos, Radius, vmr)
	local Regfile = %Volume.Regfile;
	if isnil(vmr) then
		Regfile.Next = Regfile.Next + 1;

		if (Regfile.Next > Regfile.Count) then
			Regfile.Next = 0;
		end

		vmr = Regfile[Regfile.Next];
	else
		vmr = vmr..tostring(Regfile.VMUID);
		Regfile.VMUID = Regfile.VMUID + 1;
	end

	Volume_Delete(vmr);
	Volume_AddSphere(vmr, Pos, Radius);
	return vmr;
end