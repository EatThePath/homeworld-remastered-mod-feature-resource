--To avoid resetting any state, we have an inclusion guard of sorts.
--The contents of the file being done seem to be executed in the global scope,
--not in the scope of the dofilepath call that does them. For better or worse.
if(ab_lib_inclusion == nil)then
    dofilepath("data:library/MFR/lib.lua")
end

ab_lib_inclusion = 1
