print("ab_lib_body file called")
dofilepath("data:scripts/familylist.lua")


--Divides a sob group into a table of sob groups containing individal ships.
--Was coded on the assumption that the index was the worldwide shipID but it appears to
--be local zero-based index of ships in the group based on testing. This function could
--probably be optimied a bit with that knowledge
function MFR_SobGroup_Split(sGroup)
    local workingShipIndex = 0
    local tableIndex = 0
    local totalCount = SobGroup_Count(sGroup)
    local groups = {}
    while(tableIndex < totalCount) do
        local s = MFR_CreateSafeSob()
        SobGroup_FillShipsByIndexRange(s,sGroup,workingShipIndex,1)
        if(SobGroup_Count(s) == 1)then
            groups[tableIndex] = s
            tableIndex= tableIndex+1
        end
        workingShipIndex = workingShipIndex+1
    end
    return groups
end


--return tables of individual ships groupd by a filter
function MFR_SobGroup_SplitByFilter(sInputGroup,sFilterType)
    local splitGroups = {}
    local filteredGroups = MFR_SobGroup_GroupByFilter(sInputGroup,sFilterType)
    if(filteredGroups == nil) then
        print("Filtering failed")
        return nil
    end
    for k,v in filteredGroups do
        splitGroups[k] = MFR_SobGroup_Split(v)
    end
    return splitGroups
end



--Goal here is to return a table of groups corrosponding to the options in the chosen filter.
--Family filter names often differ in capitalization from the family tables.
function MFR_SobGroup_GroupByFilter(sInputGroup,sFilterType)
    local groupNames={}
    local filteredGroups ={}
    if(sFilterType == "AttackFamily") then
        for k,v in attackFamily do
            groupNames[k]=v.name
        end
    elseif(sFilterType == "displayFamily") then
        for k,v in dockFamily do
            groupNames[k]=v.name
        end
    elseif(sFilterType == "DockFamily") then
        for k,v in dockFamily do
            groupNames[k]=v.name
        end
    else
        print("Attempted to seperate SobGroup by unknown or unimplemented filter type: "..sFilterType)
        return nil
    end
    for k,v in groupNames do
        local g = MFR_CreateSafeSob()
        SobGroup_AddFilterInclude(g,sInputGroup,sFilterType,v)
        if(SobGroup_Count(g)> 0) then
            filteredGroups[v] = g
        end
    end
    return filteredGroups
end

--Dumps a table to the log, recursive
function MFR_TableDump(sLabel, table)
    for key,value in table do
       if type(value) == "table" then
        print(sLabel.."["..key.."] is a table ")
         MFR_TableDump("--"..sLabel.."["..key.."]",value)
       elseif type(value) == "function" then
         print(sLabel.."["..key.."] is a function")
     elseif type(value) == "userdata" then
         print(sLabel.."["..key.."] is a userdata type")
       else
         print(sLabel.."["..key.."]="..value)
       end
    end
end

--Creates a sob with a unique
function MFR_CreateSafeSob()
    local name = MFR_SafeSobName()
    SobGroup_Create(name)
    return name
end
g_safeSobNameIndex = 0
function MFR_SafeSobName()
    g_safeSobNameIndex=g_safeSobNameIndex+1
    return "safesob_"..g_safeSobNameIndex
end
g_safeVolNameIndex = 0
function MFR_SafeVolName()
    g_safeVolNameIndex=g_safeVolNameIndex+1
    return "safevol_"..g_safeVolNameIndex
end