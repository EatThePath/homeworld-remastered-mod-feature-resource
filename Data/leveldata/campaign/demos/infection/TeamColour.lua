-- =============================================================================
-- Homeworld 2 Clean Data Resource Project
-- By S110
-- Version 1.0
-- 02/06/2010
-- Tools used: EditPad Lite, LuaDC version 0.9.19, and Cold Fusion LUA Decompiler v1.0.0
-- =============================================================================


teamcolours = {
	[0] = {
		{
			75/255,
			5/255,
			5/255,
		  },{
			1/255,
			1/255,
			1/255,
		  },
		"DATA:badges/TAIIDAN2_BLACK.TGA",
		{
			75/255,
			5/255,
			5/255,
		  },
		"data:/effect/trails/hgn_trail_clr.tga"
		},
	[1] = {
			{
				255/255,
				255/255,
				255/255,
			  },{
				1/255,
				25/255,
				255/255,
			  },
			"DATA:badges/TAIIDAN2_BLACK.TGA",
			{
				1/255,
				255/255,
				255/255,
			  },
			"data:/effect/trails/hgn_trail_clr.tga"
			},
		}