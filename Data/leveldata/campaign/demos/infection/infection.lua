
Race_Pass_Tags = "race_gbx"

dofilepath("data:scripts/SCAR/SCAR_Util.lua")
objectives={};

g_now_time = Universe_GameTime()
g_playerID = Universe_CurrentPlayer()
g_pointer_default = 0
g_waitID = 0
g_freightersScanned = 0
g_sbtMult=1.00001
g_scanCount=0

pings = {}

Event_Queue = {}
g_event_playing = nil


Spawns= {}
Spawns.tai_interceptor = 50
Spawns.tai_destroyer = 3


function AddEventToQueue( event_name )
	tinsert(Event_Queue,event_name)
end

function Rule_RunEventQueue()
	if(g_event_playing == nil and (getn(Event_Queue)>0) ) then
		g_event_playing = Event_Queue[1]
		Event_Start(Event_Queue[1])
	elseif( g_event_playing ~= nil and Event_IsDone(Event_Queue[1])==1 ) then
		g_event_playing = nil
		tremove(Event_Queue,1)
	end
end

function OnInit()
	print("oninit issued")
	Spawn()
end
function Spawn()
	for k,v in Spawns do
		i = 0
		while i < v do
			SobGroup_CreateShip("so_mothership",k)
			i =i +1
		end
	end
	SobGroup_FormStrikeGroup("so_ints", "X")
	SobGroup_FollowPath("so_ints","pat_patrol",1,1,0)
end
Events = {}
