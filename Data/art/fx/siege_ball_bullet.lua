fx = {
    style = "STYLE_BEAM",
    properties = {
        property_11 = {
            name = "Texture_U_Repeat",
            type = "VARTYPE_ARRAY_TIMEFLOAT",
            value = {
                0,
                1,
                1,
                1
            }
        },
        property_15 = {
            name = "LightCurve",
            type = "VARTYPE_ARRAY_TIMEFLOAT",
            value = {
                0,
                2.50000,
                0.04500,
                2.80000,
                0.06000,
                2.33333,
                0.13250,
                2.80000,
                0.14750,
                2,
                0.24625,
                2.80000,
                0.27750,
                2.40000,
                0.28750,
                1.86667,
                0.31500,
                2.86667,
                0.33375,
                2.06667,
                0.36750,
                2.46667,
                0.41125,
                1.80000,
                0.45625,
                2.20000,
                0.49125,
                1.73333,
                0.53875,
                2.40000,
                0.58000,
                1.73333,
                0.58875,
                2.33333,
                0.68250,
                1.80000,
                0.69500,
                2.46667,
                0.70500,
                1.46667,
                0.71625,
                2.13333,
                0.79125,
                1.93333,
                0.82250,
                2.53333,
                0.89625,
                1.86667,
                0.94625,
                2.53333,
                0.97500,
                1.86667,
                1,
                2.50000
            }
        },
        property_18 = {
            name = "LengthEpsilon",
            type = "VARTYPE_FLOAT",
            value = 0
        },
        property_17 = {
            name = "Arc",
            type = "VARTYPE_ARRAY_TIMEVECTOR3",
            value = {
                entry_00 = {
                    0,
                    0,
                    0,
                    0
                },
                entry_01 = {
                    1,
                    0,
                    0,
                    0
                }
            }
        },
        property_16 = {
            name = "Colour",
            type = "VARTYPE_ARRAY_TIMECOLOUR",
            value = {
                entry_00 = {
                    0,
                    0.51887,
                    0.51887,
                    0.51887,
                    1
                },
                entry_01 = {
                    1,
                    0.43396,
                    0.43396,
                    0.43396,
                    1
                }
            }
        },
        property_19 = {
            name = "WidthEpsilon",
            type = "VARTYPE_FLOAT",
            value = 0
        },
        property_09 = {
            name = "Segments",
            type = "VARTYPE_ARRAY_TIMEFLOAT",
            value = {
                0,
                1,
                1,
                1
            }
        },
        property_04 = {
            name = "CastLight",
            type = "VARTYPE_BOOL",
            value = 0
        },
        property_03 = {
            name = "Texture",
            type = "VARTYPE_STRING",
            value = "DATA:ART/FX/PLASMA_BOMB/PLASMA_BOMB.ANIM"
        },
        property_06 = {
            name = "Offset",
            type = "VARTYPE_ARRAY_TIMEVECTOR3",
            value = {
                entry_00 = {
                    0,
                    0,
                    0,
                    0
                },
                entry_01 = {
                    1,
                    0,
                    0,
                    0
                }
            }
        },
        property_05 = {
            name = "NonVisible",
            type = "VARTYPE_BOOL",
            value = 0
        },
        property_08 = {
            name = "Noise",
            type = "VARTYPE_ARRAY_TIMEFLOAT",
            value = {
                0,
                0,
                1,
                0
            }
        },
        property_07 = {
            name = "Length",
            type = "VARTYPE_ARRAY_TIMEFLOAT",
            value = {
                0,
                150,
                10,
                150
            }
        },
        property_02 = {
            name = "Blending",
            type = "VARTYPE_INT",
            value = 2
        },
        property_01 = {
            name = "Loop",
            type = "VARTYPE_BOOL",
            value = 1
        },
        property_13 = {
            name = "Light",
            type = "VARTYPE_ARRAY_TIMECOLOUR",
            value = {
                entry_00 = {
                    0,
                    0.52830,
                    0.69125,
                    1,
                    1
                },
                entry_01 = {
                    1,
                    0.52830,
                    0.69125,
                    1,
                    1
                }
            }
        },
        property_14 = {
            name = "LightWidth",
            type = "VARTYPE_ARRAY_TIMEFLOAT",
            value = {
                0,
                50,
                00.4125,
                48.0000,
                01.0250,
                55.3333,
                01.3500,
                48.0000,
                02.2250,
                44.0000,
                02.3125,
                59.3333,
                03.0125,
                55.3333,
                03.3125,
                49.3333,
                04.0625,
                55.3333,
                04.1250,
                45.3333,
                04.2250,
                62.0000,
                04.4375,
                51.3333,
                05.3625,
                55.3333,
                05.7000,
                44.0000,
                06.3875,
                45.3333,
                06.5750,
                54.0000,
                07.2500,
                52.0000,
                07.8000,
                46.0000,
                08.2625,
                46.0000,
                08.6000,
                52.0000,
                08.7750,
                45.3333,
                09.0500,
                51.3333,
                1,
                5
            }
        },
        property_20 = {
            name = "LineDistance",
            type = "VARTYPE_FLOAT",
            value = -1
        },
        property_12 = {
            name = "Width",
            type = "VARTYPE_ARRAY_TIMEFLOAT",
            value = {
                0,
                17.3333,
                00.7750,
                24.4779,
                01.3000,
                21.0052,
                01.6500,
                22.0990,
                02.1625,
                18.8177,
                02.5875,
                23.6302,
                03.3875,
                19.1458,
                04.1500,
                22.2083,
                04.3875,
                29.3177,
                04.9250,
                19.0365,
                05.1125,
                20.7865,
                05.8000,
                14.7708,
                06.3250,
                19.9115,
                06.8750,
                19.0365,
                07.4125,
                24.1771,
                08.4875,
                13.1302,
                08.9875,
                22.8646,
                10,
                19.6732
            }
        },
        property_00 = {
            name = "Duration",
            type = "VARTYPE_FLOAT",
            value = 1
        },
        property_10 = {
            name = "Texture_U_Offset",
            type = "VARTYPE_ARRAY_TIMEFLOAT",
            value = {
                0,
                0,
                1,
                0
            }
        }
    }
}
