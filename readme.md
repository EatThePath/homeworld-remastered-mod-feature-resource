# Homeworld Remastered Mod Feature Resource

**As of May 2020 this resource/wiki has been depreciated.** The content is currently being consolidated into the new [Homeworld Remastered Karos Graveyard](https://github.com/HWRM/KarosGraveyard/wiki).

The intention of this project is to be a open resource for Homeworld Remastered scripting. It aims to provide useful functions, documentation, and examples of ways to implement complex unit abilities or gameplay features.

Other mods and modders are welcome to use these features or contribute to the project. This is more rigorously covered in Docs/license.md

Documentation on how specific abilities are implemented can be found in the docs folder, as well as other notes. A general custom code template can be found in data/scripts/abilities/template.lua. Further information will be added to both in-file documentation and the project wiki over time.
