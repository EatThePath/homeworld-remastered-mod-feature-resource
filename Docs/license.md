# License

The content of this repository is to facilitate modding of Homeworld Remastered. It is based in part on property of Gearbox Software and as such may be subject to additional restrictions per applicable law. It is likely only permissable to use the content of these files within the context of a Homeworld Remastered mod. Files directly derived from Homeworld files or content are included for demonstration purposes and should be marked by a .txt file of the same name in their directory. Even if unmarked, .ship, .events, files in the art directory, or sound and image files are most likely in this category and should be treated as such unless it is otherwise clearly not the case.

Otherwise the contents of the project are covered by the below terms. In short, you can use it for whatever you want in your mod, you just need to give credit.

Copyright 2019 Homeworld Mod Feature Resource Team

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.