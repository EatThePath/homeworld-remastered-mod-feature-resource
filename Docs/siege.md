# Overview

The siege cannon project aims to recreate as much as possible the mechanics of the homeworld cataclysm siege cannon ability. This requires targeting a weapon on arbitrary points of the map, which is what the majority of the code exists to enable.

The siege cannon ability is implemented entirely through customcode and customability scripts without any extra library files needed to support them.

A testing level is implemented as a mission accessable through the extra missions button.

## Logical flow

I will walk through how this behavior works so that anyone reading the code has some context to start from.

First, a few definitions for brevity.

* 'The ship' = the ship on which the siege cannon weapon is mounted.
* 'The cursor' = the ship which is used as a targeting cursor
* CCS = Custom code script, a script called by a addCustomCode in a ship file.
* CAS = Custom ability script, a script called by a addAbility("CustomCommand") in a ship file.

The logic itself is as follows.

* On creation the ship's CCS disables the siege cannon weapon hardpoint and creates a table entry in a global table to store ability state information, using the ship's ID number as the key.

* On activation of the siege ability the ship's CAS spawns a cursor. It stores the SobGroup holding the cursor in the ship's state table

* The CAS then makes the ship unselectable. In the global table a flag for the current state of the firing sequence as waiting for target.

* The CAS finally disables the custom ability on the ship. All remaining logic is handled by the ship's CCS. This is required for proper cooldown behavior to be possible.

* The CCS update function reacts to the state table flag and begins handling behavior.

* As soon as possible the ship's CCS selects the cursor for the player.

* The player then moves the cursor to their desired target location via standard move commands.

* The player triggers the fire ability on the cursor once it is at their desired target

* The cursor's CAS makes it unselectable and deselects it, as well as issuing a stop command and then disabling its ability to move.

* The ship's CCS detects that the cursor has become unmovable and changes its state flag to firing, beginning the firing sequence.

* The ship's CCS enables its siege weapon, then attacks the stored Sobgroup containing the cursor.

* After a delay to give the siege weapon time to aim and fire the ship's CCS disables the siege weapon, cancels the attack order, and makes the ship selectable again.

* After another delay based on the maximum flight time of the siege projectile the ship's CCS kills the cursor ship. The state flag is then set to indicate the entry into the cooldown phase.

* While in the cooldown phase if the four mintues have passed since entering the firing phase, the ship's CCS re-enables the ability to allow another shot to be fired. 

* If the cursor dies(most likely scuttled) while in the waiting for target phase the ship is returned to its normal state and the ability is reenabled

## Possible improvements

There are a number of areas in which the behavior could be improved, beyond simply tuning damage numbers and making cooler effects. Listed below are the ones I've thought about and my thoughts on them.

### Damage fall-off

The original version had full damage out to a short range, then fell off steadily to no damage at a long range, past which you couldn't aim it.

This could be done by checking the distance to the cursor before firing and applying a damage multiplier to the ship, but that has the problem that if the shot hits anything early on, an iconic possibility of the siege cannon, it will be with the damage of the full range version of the shot, not whatever damage something at that shorter range should take.

The alternate possibility would be, assuming projectiles inherit multiplier effects that are applied to the firing ship while they are still in fight, to ramp down the damage multiplier on the ship over the duration of the max flight time. This has two key problems. Firstly, projectiles probably don't inherit multiplier effects that are applied to the firing ship while they are still in flight. Secondly it would make any conventional guns on the ship behave very strangely for the minute or so of flight time after every shot.

### Chargeup and firing effects
This should be fairly trivial with events and timing changes in the script. I'm not versed in events or effects, so I didn't do it.

### Better aiming
I think the ideal version of this script for gameplay use would be to have an invisible, no damage weapon with the same range as the siege cannon be enabled and used to attack the cursor as soon as it is spawned. This will mean the ship will start slewing towards the cursor immediately instead of waiting for the firing order, making for increased responsiveness to the final order. It could also be nicely dramatic visually.

This has not been implemented because the added complexity seems like it would make the script less instructive. It would need a ship with two forward facing hardpoints as the chassis.


###Better cursor arrival

Ideally the cursor would have its own launch path well outside the siege ship so that it can be spawned almost instantly and silently rather than have a hyperspace entry. This was not done so as to not involve custom hods in the demo.

It would also be a good idea to give the cursor an initial move command relative to the ship to break it out of the parade formation, especially if the better aiming point is acted on. 

###Better cursor grahpics

The cursor could have a unique tactical overlay and unique model to be more clear at communicating what it is