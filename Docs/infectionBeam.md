# Overview

The infection beam aims to recreate as much as possible the gameplay behavior of the infection beam from Homeworld Cataclysm. The most difficult part of this is the behavior of chaining through multiple targets and drawing a beam between each one in turn.

The ability is implemented through customcode and customability scripts with some functionality dependant on code in data/library, as some useful general-purpose functions were developed to support it.

A testing level is implemented as a mission accessible through the extra missions button.

## Logical Flow

Much of the behavior of the infection beam is configurable via values at the top of data/scripts/abilities/infbeam.lua

The ability is triggered through the CustomAbility functionality, which causes the handling of the first beam jump(that from the firing  ship to the first target) and then disables the ability to implement the cooldown.

When a beam jumps to a target, it is added to the ship's 'victims' table. Each victim has it's own state tracked and things are done to it by the infecting ship's CustomCode update function depending on the stage of infection it is in. At one point in the process the jump is done again, adding more ships to the victims table. The amount of 'juice' left in the beam is tracked as this happens, with different ship classes having different values. Once no targets are left in range that can be infected by the remaining juice, jumps stop.

When a jump is preformed a pair of volumes are added to the level corresponding with the positions of the ships being jumped to and from. The names of these volumes are stored in the victims table and then on the next frame a beam effect is played between these two points. Trying to draw the beam on the same frame as the points are added does not appear to work.

The function which adds the beam returns an effect index, which is stored in a table along with the time the beam was started. The update function also checks this table and ends any beams which have persisted for the configured duration.

If the infecting ship is killed while there are any active victims or beams, the victims are all immediately infected completely and the beams ended. Any remaining jumps in the beam are unfortunately wasted.

## Room for improvement

Ineveitably there are was that this effect can be improved. New beam graphics and sound effects are the most straightforward, but there are many others that would need either more code work or extensive further work in graphical assets. Those are out of the scope of this demonstration, but my thoughts on them are listed below.

### Tumbling

Currently every infected ship tumbles exactly the same way. Some form of randomness could be added to this, either from a Random function or by doing math to some other property of the ship such as its location.

### Scuttle window

I'm not certain of this but Cata may have given a chance to scuttle an infected ship. This is currently not the case in this version, but modifying the state progression of infected ships and dropping selectability partway through instead of at the start could make this possible.

### Disconnected beams

Because ships can't be made to stop instantly and beams are connected to points in space rather than ships, and because a delay of a frame is needed between setting the points and drawing the beams, it does not seem possible to remove the issue where beams will not stick to the ships. I tried a few things.

Selection_SetVelocity seemed like a good option but I have found no way to include ships in a selection.

Cutting ship speed to zero runs into issues were fighter formations bump each other and the disabled ships they just slide away since they can't stop themselves anymore.

Using shorter duration beams and recreating them every frame as ships move could work but would preclude having a nicely animated beams.

### Teamcolor blending

It's possible to set team colors via script. It might be viable to have the teamcolor of the tumbling ships be set each frame to gradually move towards the new owner's colors rather than it being sudden at the end of the duration.

## Further Infection Improvements

The graphical process of infecting the ships has been largely ignored as out of scope, since my focus was on getting gameplay behavior here. However, I've given some thoughts to ways to do it.

### Effects

The script could easily trigger an infection event on the ship in question or play an effect at it's location to get the sparking effects and so forth.

### Model replacement

Perhaps the simplest way to get the visual changes to a ship, I believe the infection beam used by the Cataclysm Remastered mod does this. With GetCoordinateSystem and SetTransform the original ship can be removed and replaced with a beastified version of the ship at the same location. Should be fairly easy to plug into this script, if given a decent table of what ships to replace with what.

### Shader blending

With a custom shader and custom textures you could have an infected and uninfected hull texture and blend between them. I believe Star Trek Continuum has a similar system for borg assimilation.

### Subsystems

Infected masses could be made as subsystems, and then spawned on ships via script. To my knowledge this can't be given an animation in the strict sense, due to issues with animated subsystems, but the sudden appearance could be disguised by particle effects or some shader tricks perhaps. Would also let you have the infected masses have their own unique shader, which could be advantageous to a visual remaster of the Beast.

This approach could also let you moderately randomize the arrangement of infected masses on the Beast ships.

You might need to be careful about z-fighting risks, however.

### Animation

Getting into elaborate territory here, you could have infected masses similar to the subsystem approach but have them buried inside the mesh of every ship, then(I believe this is possible but have not recently verified it) via code trigger animations that raise them up onto the surface slowly over the course of infection. Done right this could create the appearance of gradual growth. It is, obviously, quite labor intensive to do this for every ship.

Much like the subsystems approach this would let you have a unique infection shader, though you might need to be wary of z-fighting.

### Hybrid

A more limited custom shader might be able to change just the color of lit sections of the texture while leaning on other techniques to handle the infected masses.