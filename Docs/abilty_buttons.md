# Implementing ability buttons

Every custom code special ability in a ship file has an index parameter in it that indicates what hotkey and what GUI button it uses. Comments suggest up to 32 of these can exist. Four are defined by default

0, BTN_CMD_CUSTOM, a generic button for special abilities
1, BTN_CMD_GRAVWELL
2, BTN_CMD_DRONESACTIVATE
3, BTN_CMD_SPEEDBOOST

Currently the following have been added

4, BTN_CMD_C_CURSOR
5, BTN_CMD_C_SIEGE
6, BTN_CMD_C_INFBEAM

To add new ones to this list you need to make additions to the following files

data/ui/newui/taskbar/tb_commandpanel.lua, section starting at line 123
data/ui/commanddefines.lua, section starting at line 217
data/ui/newtaskbar.lua, section starting at line 1151

Referring to an index missing from some or all of these sections will cause either an access violation or the absence of the button.
